This repository is a bookdown website of Booklet for all the programs in EEG-CIC.

The website is hosted at [Booklet](https://eeg-cic.gitlab.io/booklet/)
